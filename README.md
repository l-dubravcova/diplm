# DIPLOMOVÁ PRÁCA - COVID-19

Kontrola bezpečnostných opatrení pomocou metód počítačového videnia.

## Obsah repozitára

**\detection** - priečinok obsahujúci detektory spolu s najlepšími natrénovanými modelmi
    
- fasterRCNN
- Haar_MobileNetV2 - detektor tvárí pomocou Haarových príznakov a následne klasifikácia rúška pomocou MobileNetV2 modelu
- models - priečinok obsahujúci natrénovaný MobileNetV2 model

- MTCNN_MobileNetV2 - detektor tvárí pomocou MTCNN a následne klasifikácia rúška pomocou MobileNetV2 modelu
- YOLOv5_streaming - kompletný kontrolný systém, ktorý streamuje video v prehliadači a deteguje na ňom rúška pomocou YOLOv5s modelu
Dôležité je inštalovať si potrebné knižnice príkazom `pip install -r requirements.txt` pred spustením akéhokoľvek detektora.

**\input** - priečinok obsahujúci vstupné video záznamy 

**\pickle_files** - priečinok, ktorý obsahoval predspracované dáta na trénovanie MobileNetV2 uložené vo formáte pickle. Kvôli prílišnej veľkosti boli odstránené. Stále sa však dajú vygenerovať zdrojovým kódom `preprocessing.ipynb` v priečinku **\training**.

**\stream_YOLOv5_docker** - priečinok obsahujúci docker file a docker-compose pre spustenie konktrolného systému v docker kontajneri

**\training** - priečinok obsahujúci zdrojové kódy používané pri trénovaní, dataset a niekoľko vygenerovaných modelov

## Použitie detektorov
- **detection/fasterRCNN**: z priečinka príkazom `python facemask_detection_FR-CNN.py` zmena detekčného modelu a vstupného súboru sa mení v zdrojom kóde na riadkoch 16 a 33
- **detection/Haar_MobileNetV2**: z priečinka príkazom `python facemask_detection_Haar.py` zmena detekčného modelu a vstupného súboru sa mení v zdrojom kóde na riadkoch 11 a 13
- **detection/MTCNN_MobileNetV2**: z priečinka príkazom `python facemask_detection_MTCNN.py` zmena detekčného modelu a vstupného súboru sa mení v zdrojom kóde na riadkoch 11 a 13
- **detection/YOLOv5_streaming**: z priečinka príkazom `python streaming_detection.py --ip 0.0.0.0 --port 8000 --input "../../input/test.mp4" --weights "weights/best.pt" --iou-thres 0.3 --conf-thres 0.6` potrebné zadefinovať potrebné vstupné parametre. Ak chceme streamovať video z webkamery, nastavíme `--input "0"`

## Spustenie docker kontajnera
- **stream_YOLOv5_docker**: z priečinka príkazom `docker-compose up` Je nutné disponovať dockerom https://docs.docker.com/get-docker/


