import matplotlib.pyplot as plt
import numpy as np
import pickle
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.utils import to_categorical

face_imgs = pickle.load(open('/Users/lubica.dubravcova/PycharmProjects/diplm/pickle_files/faces_resized_224.pickle', 'rb'))
face_labels = pickle.load(open('/Users/lubica.dubravcova/PycharmProjects/diplm/pickle_files/facemask_labels_resized_224.pickle', 'rb'))

lb_encoder = LabelEncoder()
labels = lb_encoder.fit_transform(face_labels)
labels = to_categorical(labels)

# Data preprocessing - augmentation
# Our model would never see twice the exact same picture.
# This helps prevent overfitting and helps the model generalize better.

aug = ImageDataGenerator(
    zoom_range=0.2,
    rotation_range=15,
    width_shift_range=0.1,
    height_shift_range=0.1,
    shear_range=0.15,
    fill_mode="nearest"
)

# pre-training on ImageNet
baseModel = MobileNetV2(weights="imagenet", include_top=False, input_shape=(224, 224, 3))

headModel = baseModel.output
headModel = AveragePooling2D(pool_size=(4, 4))(headModel)
headModel = Flatten(name="flatten")(headModel)
headModel = Dense(1024, activation="relu")(headModel)
headModel = Dense(1024, activation="relu")(headModel)
headModel = Dense(512, activation="relu")(headModel)
headModel = Dropout(0.2)(headModel)
headModel = Dense(3, activation="softmax")(headModel)

model = Model(inputs=baseModel.input, outputs=headModel)

for layer in baseModel.layers:
    layer.trainable = False

INIT_LR = 1e-4
EPOCHS = 100
BS = 32

keys, counts = np.unique(face_labels, return_counts=True)
dict(zip(keys, counts))

(trainX, testX, trainY, testY) = train_test_split(face_imgs, labels, test_size=0.25, stratify=labels, random_state=42)

opt = Adam(lr=INIT_LR, decay=INIT_LR / EPOCHS)
model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])

H = model.fit(
    aug.flow(trainX, trainY, batch_size=BS),
    steps_per_epoch=len(trainX) // BS,
    validation_data=(testX, testY),
    validation_steps=len(testX) // BS,
    epochs=EPOCHS,
    class_weight={0: 2., 1: 1., 2: 50.}
)

print('[INFO] evaluating network...')
predIdxs = model.predict(testX, batch_size=32)

predIdxs = np.argmax(predIdxs, axis=1)

print(classification_report(testY.argmax(axis=1), predIdxs))

N = EPOCHS
plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, N), H.history["loss"], label="trén. chybovosť")
plt.plot(np.arange(0, N), H.history["val_loss"], label="val. chybovosť")
plt.plot(np.arange(0, N), H.history["accuracy"], label="trén. presnosť")
plt.plot(np.arange(0, N), H.history["val_accuracy"], label="val. presnosť")
plt.title("Graf závislosti chybovosti a presnosti od počtu epoch")
plt.xlabel("Počet epoch")
plt.ylabel("Chybovosť/Presnosť")
plt.legend(loc="lower left")
plt.show()

model.save('model/kaggle_100epochs_4.h5')