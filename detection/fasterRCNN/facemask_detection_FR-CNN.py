import cv2
import tensorflow as tf
import numpy as np
from object_detection.utils import visualization_utils as vis_util
from object_detection.utils import label_map_util

########### USE python==1.15.0 ###############
# python facemask_detection_FR-CNN.py

num_classes=3
label_map = label_map_util.load_labelmap('model/mask_label_map.pbtxt')
categories = label_map_util.convert_label_map_to_categories(
    label_map, max_num_classes=num_classes, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

PATH_MODEL = 'model/frozen_inference_graph_10000.pb'
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.compat.v1.GraphDef()
    with tf.io.gfile.GFile(PATH_MODEL, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')
    sess = tf.compat.v1.Session(graph=detection_graph)


image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

video_capture = cv2.VideoCapture("../../input/test.mp4")

# def switch_label(arg):
#         switcher = {
#             1: "mask_worn_incorrect",
#             2: "with_mask",
#             3: "without_mask"
#         }
#         return switcher.get(arg)

# def color_for_label(arg):
#     switcher = {
#         1: (255, 0, 0),
#         2: (0, 255, 0),
#         3: (0, 0, 255)
#     }
#     return switcher.get(arg)

while True:
    ret, frame = video_capture.read()
    frame_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame_expanded = np.expand_dims(frame_rgb, axis=0)
    
    (boxes, scores, classes, num) = sess.run(
        [detection_boxes, detection_scores, detection_classes, num_detections],
        feed_dict={image_tensor: frame_expanded})

    print(np.squeeze(classes))

    vis_util.visualize_boxes_and_labels_on_image_array(
        frame,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        line_thickness=10,
        min_score_thresh=0.60)

    cv2.imshow('Facemask detector', frame)
    key = cv2.waitKey(1) & 0xFF
    if (key == ord("q")):
        break
        
video_capture.release()
cv2.destroyAllWindows()