import cv2
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
import numpy as np
from mtcnn.mtcnn import MTCNN

## python facemask_detection_MTCNN.py
#### DOCUMENTATION for MTCNN https://github.com/jbrownlee/mtcnn
detector = MTCNN()
model = load_model("../models/kaggle_100epochs_4.h5")

video_capture = cv2.VideoCapture("../../input/test.mp4")
while True:
    ret, frame = video_capture.read()
    faces = detector.detect_faces(frame)

    for face in faces:
        if face['confidence'] > 0.8:
            x,y,w,h = face['box']
            face_frame = frame[y:y+h, x:x+w]
            face_frame = cv2.cvtColor(face_frame, cv2.COLOR_BGR2RGB)
            face_frame = cv2.resize(face_frame, (224, 224))
            face_frame = img_to_array(face_frame)
            face_frame = np.expand_dims(face_frame, axis=0)
            face_frame = preprocess_input(face_frame)

            pred = model.predict(face_frame, batch_size=32)
            (without_mask, with_mask, mask_worn_incorrect) = pred[0]
            
            if with_mask > without_mask and with_mask > mask_worn_incorrect:
                label = "with mask"
                color = (0, 255, 0)
            elif without_mask > with_mask and without_mask > mask_worn_incorrect:
                label = "without mask"
                color = (0, 0, 255)
            else:
                label = "mask worn incorrect"
                color = (255, 0, 0)

            confidence = max(with_mask, without_mask, mask_worn_incorrect)
            if confidence > 0.5:
                label = "{}: {:.2f}%".format(label, confidence * 100)
                cv2.putText(frame, label, (x, y - 10),cv2.FONT_HERSHEY_SIMPLEX, 1, color, 2)
                cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
    cv2.imshow('Video', frame)
    key = cv2.waitKey(1) & 0xFF
    if (key == ord("q")):
        break
video_capture.release()
cv2.destroyAllWindows()