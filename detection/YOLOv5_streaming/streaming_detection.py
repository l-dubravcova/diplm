from flask import Response
from flask import Flask
from flask import render_template
from imutils.video import VideoStream, FileVideoStream
from models.experimental import attempt_load
from utils.datasets import letterbox
from utils.general import check_img_size, non_max_suppression
from utils.plots import plot_one_box
from utils.torch_utils import select_device, time_synchronized
import argparse
import cv2
import numpy as np
import threading
import time
import torch

outputFrame = None  # for detection

# thread secure, ensuring that one thread isn’t trying to read the frame as it is being updated
lock = threading.Lock()

app = Flask(__name__)  # initialize flask object

# CMD: python streaming_detection.py --ip 0.0.0.0 --port 8000 --input "../../input/philippines_facemasks.mp4" --weights "weights/best.pt" --iou-thres 0.3 --conf-thres 0.6
# CMD: python streaming_detection.py --ip 0.0.0.0 --port 8000 --input "0" --weights "weights/best.pt" --iou-thres 0.3 --conf-thres 0.6

@app.route("/")
def index():
	return render_template("index.html")


def detect(frame_count, source):
    global vs, outputFrame, lock
    print("source: ", source)
    
    if source == '0':
        vs = VideoStream(src=0).start()
    elif source == '1':
        vs = VideoStream(usePiCamera=1).start()
    else:
        vs = FileVideoStream(path=source).start()
    time.sleep(2.0)
    
    weights = args["weights"]
    imgsz = 864
    device = select_device(args["device"])
    half = device.type != 'cpu'
    model = attempt_load(weights, map_location=device)
    stride = int(model.stride.max())
    imgsz = check_img_size(imgsz, s=stride)
    n = 0

    while True:
        frame = vs.read()
        n += 1
        # Get names and colors
        # names = model.module.names if hasattr(model, 'module') else model.names
        names = ['mask_worn_incorrect', 'with_mask', 'without_mask']
        # BRG 1-mask_worn_incorrect 2-with_mask 3-without_mask
        colors = [[153,0,153],[0,204,0],[0,0,204]]

        # Run inference
        if device.type != 'cpu':
            model(torch.zeros(1, 3, imgsz, imgsz).to(device).type_as(next(model.parameters())))  # run once
        t0 = time.time()
        
        if n == 10: # read every 10th frame
            img = letterbox(frame, imgsz, auto=True, stride=stride)[0]
            img0 = img.copy()
            # Stack
            img = np.stack(img, 0)

            # Convert
            img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
            img = np.ascontiguousarray(img)


            # for path, img, im0s, vid_cap in dataset:
            img = torch.from_numpy(img).to(device)
            img = img.half() if half else img.float()  # uint8 to fp16/32
            img /= 255.0  # 0 - 255 to 0.0 - 1.0
            
            if img.ndimension() == 3:
                img = img.unsqueeze(0)

            # Inference
            t1 = time_synchronized()
            pred = model(img, augment=args["augment"])[0]

            # Apply NMS
            pred = non_max_suppression(pred, args["conf_thres"], args["iou_thres"], classes=args["classes"], agnostic=args["agnostic_nms"])
            t2 = time_synchronized()


            # Process detections
            for i, det in enumerate(pred):  # detections per image
                if len(det):
                    # Print results
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class

                    # Write results
                    for *xyxy, conf, cls in reversed(det):
                        label = f'{names[int(cls)]} {conf:.2f}'
                        plot_one_box(xyxy, img0, label=label, color=colors[int(cls)], line_thickness=2)

                    print(f'{label} Done. ({t2 - t1:.3f}s)')
            n = 0

            with lock:
                outputFrame = img0.copy()


# OpenCV - Stream video to web browser/HTML page
def generate():
	global outputFrame, lock
	while True:
		with lock:
			if outputFrame is None:
				continue
			(flag, encodedImage) = cv2.imencode(".jpg", outputFrame)
			if not flag:
				continue
		# the output frame in the byte format
		yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + 
			bytearray(encodedImage) + b'\r\n')

@app.route("/video_feed")
def video_feed():
	# return the response generated along with the specific media
	# type (mime type)
	return Response(generate(),
		mimetype = "multipart/x-mixed-replace; boundary=frame")

if __name__ == '__main__':
	ap = argparse.ArgumentParser()
	ap.add_argument("-i", "--ip", type=str, required=True, help="ip address of the device")
	ap.add_argument("-o", "--port", type=int, required=True, help="ephemeral port number of the server (1024 to 65535)")
	ap.add_argument("-f", "--frame-count", type=int, default=32, help="# of frames used to construct the background model")
	ap.add_argument("-in", "--input", type=str, default='source/test.mp4', help="source to stream")
	ap.add_argument('--weights', nargs='+', type=str, default='weights/best.pt', help='model.pt path(s)')
	ap.add_argument('--source', type=str, default='data/images', help='source')  # file/folder, 0 for webcam
	ap.add_argument('--conf-thres', type=float, default=0.25, help='object confidence threshold')
	ap.add_argument('--iou-thres', type=float, default=0.45, help='IOU threshold for NMS')
	ap.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
	ap.add_argument('--classes', nargs='+', type=int, help='filter by class: --class 0, or --class 0 2 3')
	ap.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
	ap.add_argument('--augment', action='store_true', help='augmented inference')
	args = vars(ap.parse_args())

	# start a thread that will perform facemask detection
	t = threading.Thread(target=detect, args=(args["frame_count"],args["input"]))
	t.daemon = True
	t.start()

	# start the flask app
	app.run(host=args["ip"], port=args["port"], debug=True,
		threaded=True, use_reloader=False)
# release the video stream
vs.stop()
